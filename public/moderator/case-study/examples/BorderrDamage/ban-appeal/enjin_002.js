$(document).ready(function(){

    // resize the standard popup overlay when resizing the window
    $(window).bind('resize', function() {
		$('body > .s_popup-canvas-separator').css({
            width: $(document).width(),
            height: $(document).height()
        });
	});
});


// show the standard popup
$.fn.enjinShowStandardPopup = function(options){
    // set the options
    options = $.extend({
        on_open_start: null,
        on_open_end: null,
        on_close_start: null,
        on_close_end: null,
        popup_top: null,
        confirm_simple_footer: null,
		get: null,
		remove_scroll: true,
		popup_class: null,
		offset_top: null
    }, options);

    // trigger the open callback
    if (options.on_open_start) {
        options.on_open_start();
    }

    // set some values
    var container = $(this);
	var popup_content;
	var css_class;
	var title;
	if(container.length === 0 && options.get) {
		popup_content = $("<div class='popup_content_placeholder progress-bar' />");
		container = popup_content;
		css_class = 'popup-progress';
		title = 'Loading';
	} else {
		popup_content = container.children().clone();
		title = container.attr('data-title');
		css_class = container.attr('class');
	}

    // remove the scroll
	if (options['remove_scroll']) {
		$('body').css('overflow', 'hidden');
	}

    // set the z-indexes
    var zindex_overlay, zindex_popup;
    var last_overlay = $('body > .s_popup-canvas-separator:visible').last();
    var last_popup = $('body > .element_popup.element_popup_core.element_popup_window:visible').last();
    if (last_popup.length > 0) {
        zindex_overlay = parseInt(last_overlay.css('z-index')) + 100;
        zindex_popup = zindex_overlay + 1;
    } else {
        zindex_overlay = 10000;
        zindex_popup = zindex_overlay + 1;
    }

    // build the overlay
    var overlay = '<div class="s_popup-canvas-separator" style="width:' + $(document).width() + 'px; ' +
        'height:' + $(document).height() + 'px; z-index:' + zindex_overlay + '"></div>';

    // build the popup
    var popup = '<div class="element_popup element_popup_core element_popup_window ' + options.popup_class + '" ' +
        'style="display:block; z-index:' + zindex_popup + '">' +
        '<div class="inner window-frame">';

    // add the title; if there's no title, then hide the header
    popup += '<div class="popup_window_title" ' + (title ? '' : 'style="display:none"') + '>' +
                '<span>' + title + '</span>' +
                '<a class="close icon-remove icon-large" href=""></a>' +
            '</div>';

	// add the main content
	popup += '<div class="content"></div>';

    // add the simple confirm footer
    if (options.confirm_simple_footer) {
        var yes = container.attr('data-confirm-yes');
        var no = container.attr('data-confirm-no');
        popup += '<div class="footer">' +
                    '<div class="element_button">' +
                        '<div class="l"></div>' +
                        '<div class="r"></div>' +
                        '<input class="confirm" type="button" value="' + yes + '">' +
                    '</div> &nbsp; ' +
                    '<div class="element_button">' +
                        '<div class="l"></div>' +
                        '<div class="r"></div>' +
                        '<input class="close" type="button" value="' + no + '">' +
                    '</div>' +
                '</div>';
    }

    // finish closing the containers
    popup += '</div>' +
    '</div>';

	var popup_window = $(popup);

	// Attach the content and set popup css and position
	popup_window.addClass(css_class);
	popup_window.find('.content').html('').append(popup_content);

	if(options.get) {
		$.get(options.get, function(response){
			container = $(response);
			popup_content.replaceWith(container.children());

			var title = container.attr('data-title');
			if(title.length) popup_window.find('.popup_window_title').show().children('span').html(title);
			else popup_window.find('.popup_window_title').hide();

			popup_window.removeClass(css_class).addClass(container.attr('class'));

			$.fn.enjinCenterStandardPopup(options, popup_window);
			$.fn.enjinBindStandardPopup(options, popup_window);
		});
	}

    // add the overlay and popup to the body
    $('body').append(overlay);
    $('body').append(popup_window);

	$.fn.enjinCenterStandardPopup(options, popup_window);
	if(!options.get) $.fn.enjinBindStandardPopup(options, popup_window);

    return container;
};

$.fn.enjinCenterStandardPopup = function(options, popup_window) {
	// get the left/top positions
	var left = ($(window).width() - popup_window.width()) * 0.5;
	var top = 0;
	if (options.popup_top) {
		if (typeof(options.popup_top) === 'function') {
			top = options.popup_top(container);       // value returned by a callback function
		} else {
			top = options.popup_top;    // direct value
		}
	} else {
		top = ($(window).height() - popup_window.height()) * 0.3;
	}
	top += $(window).scrollTop();

	if (options.offset_top) {
		top += parseInt(options.offset_top);
	}

	popup_window.css({top: top, left: left});
};

$.fn.enjinBindStandardPopup = function(options, popup_window) {
	// bind the close event
	popup_window.find('.close').bind('click', function(event){
		event.preventDefault();

		// trigger the close callback
		if (options.on_close_start) {
			options.on_close_start(popup_window);
		}

		// remove the popup
		var overlay = popup_window.prev('.s_popup-canvas-separator');
		popup_window.remove();

		// remove the overlay
		overlay.remove();

		// trigger the close callback
		if (options.on_close_end) {
			options.on_close_end();
		}

		// add the scroll back wheren there are no more popups opened
		if ($('body > .element_popup:visible').length === 0) {
			$('body').css('overflow', 'auto');
		}
	});

	// bind the confirm event
	popup_window.find('.confirm').bind('click', function(){
		options.confirm_simple_footer(popup_window);
		popup_window.enjinHideStandardPopup();
	});

	// trigger the open callback
	if (options.on_open_end) {
		options.on_open_end(popup_window);
	}
};

// hide the standard popup
$.fn.enjinHideStandardPopup = function(){
    $(this).find('.popup_window_title .close').trigger('click');
    return $(this);
};

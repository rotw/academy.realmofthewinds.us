var Forum = function( ){
	var instance = this;
	
	$(document).ready(function() {
		instance.autoImgResize();
		
		$('.post-signature.scroll-hover').hover( function() {$(this).css({overflow:'auto'});}, function() {$(this).css({overflow:'hidden'});} );
	});
}
Forum.prototype = {
	
	checkCanPost: function() {
		if ( typeof this.require_game !== 'undefined' ) {
			Enjin_Core.alert("This forum requires an added "+this.require_game+" character to post.<br/>Go to the characters page on your profile to add your character.");
			return false;
		}
		return true;
	},
	
	showActionBox: function(event)
	{
		var forumModule = $(this).closest('.m_forum');
		var forum_id = forumModule.attr('forum_id');
		var popupBox = forumModule.find('.moderator-popup');
		var submit = popupBox.find('input.submit');
		var select = popupBox.find('select.move-location');
		var input = popupBox.find('div.text-field');
		
		select.hide();
		input.hide();
		
		// Deleting post
		if($(this).hasClass('link-delete-post'))
		{
			submit.val('Delete');
			popupBox.find('.message').html('Delete this post?');
			
			var post = $(this).closest('.row');
			
			submit.unbind("click").click(function(event) {
				forum.deletePost( post.attr('post_id') );//post);
				popupBox.fadeOut(160);
			});
		}
		
		// Sticky thread
		if($(this).hasClass('link-sticky-thread'))
		{
			submit.val('Sticky');
			popupBox.find('.message').html('Make this thread sticky?');
			
			submit.unbind("click").click(function(event) {
				forum.moderateThread("sticky-thread", "sticky", location.href);
				popupBox.fadeOut(160);
			});
		}
		else if($(this).hasClass('link-unsticky-thread'))
		{
			submit.val('Unsticky');
			popupBox.find('.message').html('Unsticky this thread?');
			
			submit.unbind("click").click(function(event) {
				forum.moderateThread("sticky-thread", "normal", location.href);
				popupBox.fadeOut(160);
			});
		}
		
		// Lock thread
		if($(this).hasClass('link-lock-thread'))
		{
			submit.val('Lock');
			popupBox.find('.message').html('Lock this thread?');
			
			submit.unbind("click").click(function(event) {
				forum.moderateThread("lock-thread", "locked", location.href);
				popupBox.fadeOut(160);
			});
		}
		else if($(this).hasClass('link-unlock-thread'))
		{
			submit.val('Unlock');
			popupBox.find('.message').html('Unlock this thread?');
			
			submit.unbind("click").click(function(event) {
				forum.moderateThread("lock-thread", "normal", location.href);
				popupBox.fadeOut(160);
			});
		}
		
		// Move thread
		if($(this).hasClass('link-move-thread'))
		{
			submit.val('Move');
			
			if(select.html() == "")
			{
				popupBox.find('.message').html('Move thread to forum:<span style="margin: 13px 0px -7px 0px; display: block; color: gray;">Loading...</span>');

				$.getJSON(Forum.URL.getModerationList, function(json){
					var options = '';
					var category_id = null;

					for (var i in json)
					{
						if(json.hasOwnProperty(i)) {
							if (category_id != json[i].category_id) {
								options += '<option disabled value="">------------------------------------------------------------------</option>';
								options += '<option disabled style="font-weight: bold;" value="">' + Enjin_Core.filterOutput(json[i].category_name) + '</option>';
								category_id = json[i].category_id;
							}

                            var same_forum = (json[i].forum_id == forum_id ? ' disabled' : '');
                            var sub_forum = (json[i].parent_id != null ? ' - ' : '');

							var nestpx = parseInt(json[i].nest_level, 10) * 14;
							options += '<option' + same_forum + ' style="padding-left: ' + nestpx + 'px;" value="' + json[i].forum_id + '">' +
                                sub_forum + Enjin_Core.filterOutput(json[i].forum_name) + '</option>';
						}
					}

					if(options == "") options = "<option value='0'>-- You do not have access to other forums --</option>";
					
					select.html(options);
					select.show();
					popupBox.find('.message').html('Move thread to forum:');
				});
			}
			else
			{
				select.show();
				popupBox.find('.message').html('Move thread to forum:');
			}
			
			submit.unbind("click").click(function(event) { 
				forum.moderateThread("move-thread", select.val(), Forum.URL.returnURL);
				popupBox.fadeOut(160);
			});
		}
		
		// Delete thread
		if($(this).hasClass('link-delete-thread'))
		{
			submit.val('Delete');
			popupBox.find('.message').html('Delete entire thread?');
			
			submit.unbind("click").click(function(event) {
				forum.moderateThread("delete-thread", "", Forum.URL.returnURL);
				popupBox.fadeOut(160);
			});
		}
		
		
		/*
		 * Generic popup box functionality
		 */
		
		popupBox.fadeIn(160);
		
		popupBox.find('a.cancel').click(function(event) {
			popupBox.fadeOut(160);
			popupBox.find('select.move-location').hide();
		});
		
		if ($(this).hasClass('link-delete-post')) {
			popupBox.css({
				top: event.pageY - forumModule.offset().top + 20,
				left: event.pageX - forumModule.offset().left - popupBox.width() + 10
			});
        } else {                       
			var thread_moderation = $(this).parents('.forum-area:first .thread-moderation');
			var top = thread_moderation.position().top + thread_moderation.height();
            var left = $(this).position().left; // because #page has margin auto, offset() doesn't work properly
			popupBox.css({
				top: top,
				left: left
			});
		}
		
		
		event.stopPropagation();
		
		$(document).one("click", function(event) {
			popupBox.fadeOut(160);
			popupBox.find('select.move-location').hide();
		});
		
		popupBox.bind('click', function(event) {
			event.stopPropagation();
		});
		
		return false;
	},
	
	deletePost: function(post)
	{
		if ( post instanceof Array ) {post = post.join(',');}
		
		$.post(location.href, {m: Forum.preset_id, op: "delete-post", post_id: post},
			function(data){ 
				if(data == 'success')
				{
					location.reload(true);
				}
			}
		);
		
	},
	
	/**
	 *
	 */
	showBulkModerateTools: function()
	{
        $('div .bulk-moderator-tool').css( {'display':'block'} );
        $('input:checkbox.bulk-moderator-item').css( {'display':'inline'} ); 
        $('input:checkbox.bulk-moderator-item-all').css( {'display':'inline'} );         

		$('input:checkbox.bulk-moderator-item').click( function() {forum.updateModeratorPostCount();} );
        $('input:checkbox.bulk-moderator-item-all').click( function() {forum.selectAllItems($(this).is(':checked'));} ); 
        
        return false;
	},
	
	
	/**
	 *
	 */
	hideBulkModerateTools: function()
	{
        $('div .bulk-moderator-tool').css( {'display':'none'} );
        $('.bulk-moderator-count').text( '0' );
        $('input:checkbox.bulk-moderator-item').css( {'display':'none'} ).attr('checked', false); 
        $('input:checkbox.bulk-moderator-item-all').css( {'display':'none'} ).attr('checked', false); 
        
        return false;
	},
    
    selectAllItems: function( isChecked )
    {
        $('input:checkbox.bulk-moderator-item').prop('checked', isChecked);
        this.updateModeratorPostCount();
    },
    
    updateModeratorPostCount: function()
    {
        $('.bulk-moderator-count').text( this.getModeratorPostCount() );
	},
	
	getModeratorPostCount: function()
	{
	    return $('input:checked.bulk-moderator-item').length;
	},
	
	/**
	 *
	 */
	applyModeratorAction: function( button, event)
	{
		var forumModule = button.closest('.m_forum');
		var forum_id = forumModule.attr('forum_id');
		var popupBox = forumModule.find('.moderator-popup');
		var submit = popupBox.find('input.submit');
		var select = popupBox.find('select.move-location');
		var mergeType = popupBox.find('select.merge-type');
		var input = popupBox.find('div.text-field');
		
		select.hide();
		input.hide();
        mergeType.hide();
                
		var posts = [];
		$('input:checked.bulk-moderator-item').each( function() {posts.push($(this).val());} );
        
	    switch ( $('.bulk-moderator-tool select').val() )
	    {
	        case "delete-post":
        	    if ( this.getModeratorPostCount() == 0 ) 
        	    {
        	        Enjin_Core.showMessagePopup({message:'Please select one or more posts to delete'});
        	        return;
        	    }   
        	    
	    		submit.val('Delete');
    			popupBox.find('.message').html('Delete all selected posts?');
    			
    			submit.unbind("click").click(function(event) {
    				forum.deletePost( posts );
    				popupBox.fadeOut(160);
    			});
	        break;   
	        
	        case "move-post":
        	    if ( this.getModeratorPostCount() == 0 ) 
        	    {
        	        Enjin_Core.showMessagePopup({message:'Please select one or more posts to move'});
        	        return;
        	    }   
        	    
	    		submit.val('Move');
    			popupBox.find('.message').html('Enter URL of the thread to move posts to:');
    			input.show();
    			
    			submit.unbind("click").click(function(event) {
    				forum.moderateThread("move-post", $('div.text-field input').val(), document.location.href, posts);
    				popupBox.fadeOut(160);
    			});	       
    		 break;   
    		 
    		 case "delete-thread":
        	    if ( this.getModeratorPostCount() == 0 ) 
        	    {
        	        Enjin_Core.showMessagePopup({message:'Please select the threads to delete'});
        	        return;
        	    }      		 
        	    
	    		submit.val('Delete');
    			popupBox.find('.message').html('Delete all selected threads?');
    			
    			submit.unbind("click").click(function(event) {
    				forum.moderateThread("delete-thread", "", Forum.URL.returnURL, posts);
    				popupBox.fadeOut(160);
    			});
    		 break;

    		 case "move-thread":
        	    if ( this.getModeratorPostCount() == 0 ) 
        	    {
        	        Enjin_Core.showMessagePopup({message:'Please select the threads to move'});
        	        return;
        	    }      		 

				popupBox.find('.message').html('Move threads to forum:<span style="margin: 13px 0px -7px 0px; display: block; color: gray;">Loading...</span>');

				$.getJSON(Forum.URL.getModerationList, function(json){
					var options = '';
					var category_id = null;

					for (var i in json)
					{
						if(json.hasOwnProperty(i)) {
							if (category_id != json[i].category_id) {
								options += '<option disabled value="">------------------------------------------------------------------</option>';
								options += '<option disabled style="font-weight: bold;" value="">' + json[i].category_name + '</option>';
								category_id = json[i].category_id;
							}
                            var same_forum = (json[i].forum_id == forum_id ? ' disabled' : '');
                            var sub_forum = (json[i].parent_id != null ? ' - ' : '');

							var nestpx = parseInt(json[i].nest_level, 10) * 14;
							options += '<option' + same_forum + ' style="padding-left: ' + nestpx + 'px;" value="' + json[i].forum_id + '">' + sub_forum + json[i].forum_name + '</option>';
						}
					}

					if(options == "") options = "<option value='0'>-- You do not have access to other forums --</option>";

					select.html(options);
					select.show();
					popupBox.find('.message').html('Move thread to forum:');
				});

	    		submit.val('Move');
    			submit.unbind("click").click(function(event) {
    				forum.moderateThread("move-thread", select.val(), Forum.URL.returnURL, posts);
    				popupBox.fadeOut(160);
    			});
    		 break;

    		 case "sticky-thread":
        	    if ( this.getModeratorPostCount() == 0 ) 
        	    {
        	        Enjin_Core.showMessagePopup({message:'Please select the threads to move'});
        	        return;
        	    }      		 
        	    
	    		submit.val('Make Sticky');
    			popupBox.find('.message').html('Make selected threads sticky?');
    			
    			submit.unbind("click").click(function(event) {
    				forum.moderateThread("sticky-thread", "sticky", location.href, posts );
    				popupBox.fadeOut(160);
    			});
    		 break;    		

    		 case "unsticky-thread":
        	    if ( this.getModeratorPostCount() == 0 ) 
        	    {
        	        Enjin_Core.showMessagePopup({message:'Please select the threads to move'});
        	        return;
        	    }      		 
        	    
	    		submit.val('Make Unsticky');
    			popupBox.find('.message').html('Remove the sticky status on selected threads?');
    			
    			submit.unbind("click").click(function(event) {
    				forum.moderateThread("sticky-thread", "normal", location.href, posts );
    				popupBox.fadeOut(160);
    			});
    		 break;        		  

    		 case "lock-thread":
        	    if ( this.getModeratorPostCount() == 0 ) 
        	    {
        	        Enjin_Core.showMessagePopup({message:'Please select the threads to lock'});
        	        return;
        	    }      		 
        	    
	    		submit.val('Lock');
    			popupBox.find('.message').html('Lock the selected threads?');
    			
    			submit.unbind("click").click(function(event) {
    				forum.moderateThread("lock-thread", "locked", location.href, posts );
    				popupBox.fadeOut(160);
    			});
    		 break; 

    		 case "unlock-thread":
        	    if ( this.getModeratorPostCount() == 0 ) 
        	    {
        	        Enjin_Core.showMessagePopup({message:'Please select the threads to move'});
        	        return;
        	    }      		 
        	    
	    		submit.val('Unlock');
    			popupBox.find('.message').html('Unlock the selected threads?');
    			
    			submit.unbind("click").click(function(event) {
    				forum.moderateThread("lock-thread", "normal", location.href, posts );
    				popupBox.fadeOut(160);
    			});
    		 break;     	

    		 case "merge-thread":
        	    if ( this.getModeratorPostCount() < 2 ) 
        	    {
        	        Enjin_Core.showMessagePopup({message:'Please select at least 2 threads to merge'});
        	        return;
        	    }      		 

        	    var options = select.prop('options'); 
        	    options.length = 0;
        	    $('input:checked.bulk-moderator-item').each( function() {var t = $(this).attr('data-thread');          
        	                                                              if(t.length > 56) {t = t.substr(0,56)+'...';}; 
        	                                                              options[options.length] = new Option(t, $(this).attr('value')); 
        	                                                            }); 

	    		submit.val('Merge');
	    		select.show();
	    		mergeType.show();
	    		
    			popupBox.find('.message').html('Select main thread to merge to:');
    			
    			submit.unbind("click").click(function(event) {
    				forum.mergeThreads( select.val(), mergeType.val(), posts, location.href );
    				popupBox.fadeOut(160);
    			});
    		 break;     	    		 
    		 	              
             default:return;   	    	 
        }	        
		
		
		/*
		 * Generic popup box functionality
		 */
		
		popupBox.fadeIn(160);
		
		popupBox.find('a.cancel').click(function(event) {
			popupBox.fadeOut(160);
			popupBox.find('select.move-location').hide();
			popupBox.find('div.text-field').hide();
		});
		
    	popupBox.css({
				top: event.pageY - forumModule.offset().top + 20,
				left: event.pageX - forumModule.offset().left - (popupBox.width()/2)
			});
		
		event.stopPropagation();
		
		$(document).one("click", function(event) {
			popupBox.fadeOut(160);
			popupBox.find('select.move-location').hide();
			popupBox.find('div.text-field').hide();
		});
		
		popupBox.bind('click', function(event) {
			event.stopPropagation();
		});
		
		return false;
	},              
	
	/**
	 * 
	 * @param array itemList Optional list of threads/posts to operate on if not current thread
	 */
	moderateThread: function(operation, setting, gotoURL, itemList )
	{
	    var params = {m: Forum.preset_id, op: operation, setting: setting};
	    if ( itemList instanceof Array ) {params.threads = itemList.join(',');}
		$.post(location.href, params,
			function(data){
				if ( data == 'success' ) 
				{
				    location.reload(true);
                }				    
                else
                {
                    Enjin_Core.showMessagePopup( {message:data} );
                }
			}
		);
	},
	
	/**
	 * Applies the merge threads moderate action, seperate to moderateThread() as it has extra params
	 */
	mergeThreads: function( mainThreadId, mergeType, threads, gotoURL )
	{
	    var params = {m: Forum.preset_id, op: 'merge-thread', mergeType: mergeType, mainThreadId:mainThreadId, threads: threads.join(',')};
		$.post(location.href, params,
			function(data){
				if(data == 'success') document.location = gotoURL;
			}
		);	    
	},
	
	showPreview: function(preset_id, type, element)  {
		var info;
		var data = {};
		
		switch(type) {
			case 'thread':
				info = this.showPreviewThread(element);
				break;
			case 'reply':
				info = this.showPreviewReply(element);
				break;
			case 'edit':
				info = this.showPreviewEdit(element);
				break;				
		}
		
		data = info.data;
		data.preset_id = preset_id;
        data.forum_id = $(element).attr('data-forum-id');

		$.post('/ajax.php?s=bbparser&f=preview-post', data,
				function(response){
					info.element.html(response);
					info.element_parent.show();
					// hack for ffxivarr items to work in the post preview
					if ( typeof fPopLoadItem === 'function' ) {
						fPopLoadItem();
					}
					// SWTOR - torcommunity needs to have the tooltips inited so they can show right
					if ( typeof initTooltips === 'function' ) {
						initTooltips();
					}
				}
		);
	},

	showPreviewThread: function(element) {
		var info = {
			data: {},
			element: null
		};
		
		var parent = $(element).closest('.forum-area');
		
		info.element = parent.find('.preview .post-content');
		info.element_parent = parent.find('.preview');
		info.data.content = parent.find('.thread textarea[name=content]').val();

		return info;
	},
	
	showPreviewReply: function(element) {
		var info = {
			data: {},
			element: null
		};
		
		var parent = $(element).closest('.forum-area');
		
		info.element = parent.find('.preview .post-content');
		info.element_parent = parent.find('.preview');
		info.data.content = parent.find('.reply textarea[name=content]').val();
		
		return info;
	},
	
	showPreviewEdit: function(element) {
		var info = {
			data: {},
			element: null
		};
		
		var parent = $(element).closest('.forum-area');
		
		info.element = parent.find('.preview .post-content');
		info.element_parent = parent.find('.preview');
		info.data.content = parent.find('.post textarea[name=content]').val();
		
		return info;
	},
	
	/**
	 * This is used by the voting system, its hide show works in a slightly illogical way for
	 * the admin hide/show, so rather than break stuff the toggleShowHide is used for that
	 */
	toggleHiddenPost: function( el, post_id )
	{
	    var post = $('tr[post_id=' + post_id +']');
	    if ( post.hasClass('hidden') )
	    {
			$('tr[data-hidden-post-trigger=' + post_id + ']').hide();
	        post.removeClass('hidden');
	        el.text('Hide Post');
        }
        else
        {
	        post.addClass('hidden');
	        el.text('View Post');
        }	        
	},

	toggleShowHidePost: function( el, post_id )
	{
	    var post = $('tr[post_id=' + post_id +']');
	    if ( post.hasClass('hidden') )
	    {
			$('tr[data-hidden-post-trigger=' + post_id + ']').hide();
	        post.removeClass('hidden');
        }
        else
        {
			$('tr[data-hidden-post-trigger=' + post_id + '] .link a').html('View Post');			
			$('tr[data-hidden-post-trigger=' + post_id + ']').show();
	        post.addClass('hidden');
        }	
	},	
		
	autoImgResize: function()
	{		
		var self = this;

		$('.m_forum .post-content img.bbcode_img, .m_forum .post-attachments-images img').each(function(i){
			if (this.complete) {
				self.autoImgResizeItem(this);
			} else {
				var image = this;				
				$(this).bind('load', function() {
					self.autoImgResizeItem(image);
				});
			}
		});
	},
	
	autoImgResizeItem: function(image) {
		if(image.width >= 610)
		{ 
			var myParent = $(image).parent().get(0);
			if(myParent.tagName != 'A')
			{
				image.style.cursor = 'pointer';
				image.onclick = function(e) {
					if(window.event)
						window.open(window.event.srcElement.src, 'enjinImg', 'menubar=no, toolbar=no, location=no, directories=no, fullscreen=no, titlebar=yes, hotkeys=no, status=no, scrollbars=yes, resizable=yes, width=800, height=600');
					else
						window.open(e.target.src, 'enjinImg', 'menubar=no, toolbar=no, location=no, directories=no, fullscreen=no, titlebar=yes, hotkeys=no, status=no, scrollbars=yes, resizable=yes, width=800, height=600');
				}
			}
		}		
	},
	
	
	//poll part
	poll_inited: {},
	poll_template: null,
	poll_count: {},
	poll_max_answers: 25,
	
	initPoll: function(preset_id, element) {
		//get template
		var pt = element.find('.area-poll .answers .template');
		pt.removeClass('template');
		this.poll_template = pt.clone();
		pt.remove(); //don't show it
		
		this.poll_count[preset_id] = 1;
		this.poll_inited[preset_id] = true;
		
		if (forum_data 
			&& typeof forum_data[preset_id] != 'undefined') {
			var self = this;
			
			jQuery.each(forum_data[preset_id], function(key, value) {
				self.addAnswerPoll(preset_id, element, value);
			});
			
		} else {
			//add first elements
			this.addAnswerPoll(preset_id, element); 
			this.addAnswerPoll(preset_id, element);
		}
	},
	
	addAnswerPoll: function(preset_id, element, value) {		
		var main_form = $(element).closest('form[name=postform]').find('.area-poll .answers');
		var nelement = this.poll_template.clone();
		var count_id = this.poll_count[preset_id]++;
				
		nelement.addClass('answer-'+count_id);
		nelement.find('.text-labeling .count').html(count_id);
		nelement.find('.input-text input[type=text]').attr('name', 'poll_answer_'+count_id);
		
		if (value)
			nelement.find('.input-text input[type=text]').val(value);
		
		main_form.append(nelement);
		
		if (count_id < 3) {
			//don't show close
			main_form.find('.answer .close').hide();
		} else {
			//show close button
			main_form.find('.answer .close').show();
		}
		
		if (count_id >= this.poll_max_answers) {
			//don't allow more
			$(element).closest('form[name=postform]').find('.area-poll .answer-add').hide();			
		}
	},
	
	removeAnswerPoll: function(preset_id, element) {
		var area_poll = $(element).closest('form[name=postform]').find('.area-poll');
		var main_form =  area_poll.find('.answers');
		var count_id=1;
		var nelement = $(element).closest('.answer');
		nelement.remove();
		
		this.poll_count[preset_id]--;
		
		main_form.find('.answer').each(function() {			
			$(this).find('.text-labeling .count').html(count_id);
			$(this).find('.input-text input[type=text]').attr('name', 'poll_answer_'+count_id);
			count_id++;
		});
		
		if (count_id <= 3) {
			main_form.find('.answer .close').hide();
		}
		
		if (count_id <= this.poll_max_answers) {
			area_poll.find('.answer-add').show();
		}
	},
	
	initPostPoll: function(preset_id) {
		var element = $('.m_forum_'+preset_id+' .area-poll');
		this.showPoll(preset_id, element);
	},
	
	showPoll: function(preset_id, element) {
		var main_form = $(element).closest('form[name=postform]');
		
		if (!this.poll_inited[preset_id])
			this.initPoll(preset_id, main_form);
				
		//main_form.find('.text-labeling.body').html('Body and Poll Question');
		main_form.find('.area-poll').show();
		main_form.find('.buttons-area .poll .element-remove').show();
		main_form.find('.buttons-area .poll .element-add').hide();		
		main_form.find('input[name=poll_have]').val('1');
	},
	
	removePoll: function(preset_id, element) {
		var main_form = $(element).closest('form[name=postform]');
		//main_form.find('.text-labeling.body').html('Body');
		main_form.find('.area-poll').hide();
		main_form.find('.buttons-area .poll .element-remove').hide();
		main_form.find('.buttons-area .poll .element-add').show();
		main_form.find('input[name=poll_have]').val('0');
	},
	
	
	/* view part */
	showViewResultsPoll: function(element) {
		var main_form = $(element).closest('.post-poll-area');
		
		main_form.find('.view-poll').show();
		main_form.find('.post-poll').hide();
	},
	
	hideViewResultsPoll: function(element) {
		var main_form = $(element).closest('.post-poll-area');
		
		main_form.find('.view-poll').hide();
		main_form.find('.post-poll').show();
	},
	
	clearThreadGhost: function(event)
	{
		var thread_id = $(this).attr('thread_id');
		
		$.post(location.href, {m: Forum.preset_id, op: "clear-thread-ghost", thread_id: thread_id},
			function(data){
				if(data == 'success') location.reload(true);
			}
		);
		
		return false;
	},
	
	markForumRead: function(event)
	{
		var forum_id = $(this).attr('forum_id');
		
		var op = "mark-forum-read";
		if(forum_id == 'all') op = "mark-forums-read";
		
		$.post(location.href, {m: Forum.preset_id, op: op, forum_id: forum_id},
			function(data){
				if(data == 'success') location.reload(true);
			}
		);
		
		 $(this).replaceWith("<span>Marking...</span>");
		
		return false;
	},
	
	/**
	 * 
	 */
	markThreadUnread: function(event)
	{
		var thread_id = $(this).attr('data-threadid');
		
		var op = "mark-thread-unread";		
		$.post(location.href, {m: Forum.preset_id, op: op, thread_id: thread_id},
			function(data){
				if (data == 'success') location.reload(true);
			}
		);
		
		 $(this).val("Marking...");
		
		return false;
	},
	
	/**
	 * 
	 */
	markThreadRead: function(event)
	{
		var thread_id = $(this).attr('data-threadid');
		
		var op = "mark-thread-read";		
		$.post(location.href, {m: Forum.preset_id, op: op, thread_id: thread_id},
			function(data){
				if (data == 'success') location.reload(true);
			}
		);
		
		 $(this).val("Marking...");
		
		return false;
	},
			
	search: function(text, page, mode, mode_id)
	{
		$.ajax({
    		type: "POST",
    		url: "/ajax.php?s=forum",
    		data: {cmd: 'search', preset_id: Forum.preset_id, q: text, page: page, mode: mode, mode_id: mode_id},
    		dataType: "json",
    		success: function(data){
    			if(data.success == 'false') alert(data.error);
    			else {
    				forum.displaySearchResults(data);
    			}
    		},
    		error: function (XMLHttpRequest, textStatus, errorThrown) {
    			alert('Error', 'There was an error performing the search, please try again later.');
    		}
    	});
	},
	
	clearSearch: function() {
		$('.forum-area.search-results').remove();
		$('.m_forum .breadcrumbs.search').hide();
		$('.m_forum .breadcrumbs.standard').show();
		$('.forum-area.forum-content').show();
	},
	
	displaySearchResults: function(data)
	{
		data.page = parseInt(data.page);
		data.pages = parseInt(data.pages);
		
		$('.forum-area.forum-content').hide();
		$('.forum-area.search-results').remove();
		$('.m_forum .breadcrumbs.standard').hide();
		$('.m_forum .breadcrumbs.search').show();
		
		var searchtext = "Search";
		if(data.mode == 'forum') searchtext = "Search Forum";
		else if(data.mode == 'thread') searchtext = "Search Thread";
		
		var searchbox = "<form onsubmit='forum.search($(this).find(\"input[name=search]\").val(), 1, \"" + data.mode + "\", \"" + (data.mode_id ? data.mode_id : '') + "\"); Enjin_Core.disableButton($(this).find(\"input[type=submit]\"), 4, \"Searching...\"); return false;'><div class='search-box'>\
				<div class='element_button'><div class='l'><!-- --></div><div class='r'><!-- --></div><input type='submit' value='" + searchtext + "'></div>\
				<div class='input-text'>\
					<div class='tl'></div><div class='tr'></div><div class='bl'></div><div class='br'></div>\
					<input name='search' maxlength='100' value=\"" + data.q + "\">\
				</div>\
			</div></form>";
		
		var pagewidget = "";
		if(data.pages > 1)
		{
			pagewidget = "<div class='element_pagewidget search-pages'>\
				<span class='text'>Page</span>\
				<div class='input-text'><input value='" + data.page + "' maxlength='3'></div>\
				<div class='element_smallbutton'><div class='l'></div><div class='r'></div><input type='button' onclick='forum.search($(this).parent().parent().parent().parent().attr(\"q\"), $(this).parent().siblings(\".input-text\").children(\"input\").val(), \"" + data.mode + "\", \"" + (data.mode_id ? data.mode_id : '') + "\"); Enjin_Core.disableButton(this, 4);' value='Go'></div>\
				<span class='text rightmost'>of " + data.pages + "</span>";
			if(data.page > 1)
				pagewidget += "<div class='element_smallbutton'><div class='l'></div><div class='r'></div><input type='button' onclick='forum.search($(this).parent().parent().parent().parent().attr(\"q\"), " + (data.page - 1) + ", \"" + data.mode + "\", \"" + (data.mode_id ? data.mode_id : '') + "\"); Enjin_Core.disableButton(this, 4);' class='left' value='<'></div>";
			if(data.page < data.pages)
				pagewidget += "<div class='element_smallbutton'><div class='l'></div><div class='r'></div><input type='button' onclick='forum.search($(this).parent().parent().parent().parent().attr(\"q\"), " + (data.page + 1) + ", \"" + data.mode + "\", \"" + (data.mode_id ? data.mode_id : '') + "\"); Enjin_Core.disableButton(this, 4);' class='left' value='>'></div>";
			pagewidget += "</div>";
		}
		
		
		var html = "<div class='contentbox results'>\
			<div class='block-title'>\
				<div class='left'><!-- --></div>\
				<div class='right'><!-- --></div>\
				<div class='text'><span class='mask'>Found " + data.found + " entries for <span class='highlighted'>\"" + data.q + "\"</span></span></div>\
			</div>\
			<div class='block-container'>\
				<div class='he l'><!--  --></div>\
				<div class='he r'><!--  --></div>\
				<div class='he tl'><!--  --></div>\
				<div class='he tr'><!--  --></div>\
				<div class='he bl'><!--  --></div>\
				<div class='he br'><!--  --></div>\
				<div class='structure'>";
		
		for(var i=0; i<data.results.length; i++)
		{
			if(i == 0) html += "<div class='search-result-post first'>";
			else html += "<div class='search-result-post'>";
			
			html += "<div class='result-subject'><a href='" + Forum.baseurl + "/viewthread/" + data.results[i].thread_id + "/post/" + data.results[i].post_id + "#p" + data.results[i].post_id + "' target='_blank'>" + data.results[i].thread_subject + "</a></div>";
			html += "<div class='result-content'>" + data.results[i].post_content + "</div>";
			html += "<div class='result-info'><a class='forum-name' href='" + Forum.baseurl + "/viewforum/" + data.results[i].forum_id + "'>" + data.results[i].forum_name + "</a> &nbsp;&middot;&nbsp; " + (parseInt(data.results[i].thread_replies) + 1) + " posts &nbsp;&middot;&nbsp; Posted " + data.results[i].post_time + " &nbsp;&middot;&nbsp; By " + data.results[i].username +  "</div>"
			html += "</div>";
		}
		html += "</div></div></div>";
		
		$('.m_forum').append("<div class='forum-area search-results' q=\"" + data.q + "\"></div>");
		$('.forum-area.search-results').append("<div class='above-forum'>" + searchbox + pagewidget + "<br></div>" + html);
	},
	
	showAvatarHover: function( event )
	{
	    var el = $(event.currentTarget).closest('.avatar-hover-trigger');
	    
	    var userId = el.attr('data-userid');
	    var registeredId = parseInt(el.attr('data-registeredid'),10);
	    var siteId = el.attr('data-siteid');
	    var joinDate = el.attr('data-date');
	    var postCount = parseInt(el.attr('data-postcount'),10);
	    var userIP = el.attr('data-ip');
	    var adminFeatures = parseInt(el.attr('data-admin'),10);
	    var isOwner = parseInt(el.attr('data-owner'),10);
	    var username = el.attr('data-username');
	    var postId = el.attr('data-id');
	    var votes = parseInt(el.attr('data-votes'),10);;
	    var displayVotes = parseInt(el.attr('data-showvotes'),10);
	    
        var items = [ ['<a href="/profile/' + userId + '" class="menu-link">Profile</a>','html'] ];
        
        // only show messages if there is a registered user viewing the site
        if ( registeredId )
        {
	        items.push( ['<a href="/dashboard/messages/compose?type=user&id=' + userId + '" class="menu-link">Message User</a>','html'] );
        }
        	                 
	    items.push( ['<a href="/profile/' + userId + '/posts" class="menu-link">View User\'s Posts</a>','html'] );
	    items.push( ['','divider'] );	                 
                            
        if ( adminFeatures && userIP != undefined ) {items.push( ['<span class="menu-link">' + userIP + '</span>','html'] );}
                        
        items.push( ['<span  class="menu-link">Joined on ' + joinDate + '</span>','html'] );
        
        // only show a post count if there is one available
        if ( postCount >= 0 ) {items.push( ['<span class="menu-link">' + postCount + ' posts</span>','html'] );}
        
        if ( displayVotes )
        {                                          
            var text = ( votes > 0 ? '+' : '' ) + el.attr('data-votes') + ' Votes';
            items.push( ['<span  class="menu-link ' + ( votes > 0 ? 'votes-positive' : (votes < 0 ? 'votes-negative' : '') ) + '">' + text + '</span>','html'] ); 
        }
                        
        // the ban actions are only available to the administrators, however the clear user votes action
        // is available to administrators as well the normal user if he is hovering his own avatar
        if ( adminFeatures ) 
        { 
            items.push( ['','divider'] );

			if(userIP != undefined)
				items.push( ['<div data-id="'+postId+'" data-userid="'+userId+'" data-ip="'+userIP+'" data-username="'+username+'" class="menu-link ip-popup">'+
                    '<a href="#" class="ban-user">Ban User</a>&nbsp;&middot;&nbsp;'+
                    '<a href="#" class="ban-ip">Ban IP</a>&nbsp;&middot;&nbsp;'+
                    '<a href="#" class="clear-votes">Clear Votes</a>'+
                '</div>','html'] );
			else
				items.push( ['<div data-id="'+postId+'" data-userid="'+userId+'" data-username="'+username+'" class="menu-link ip-popup"><a href="#" class="ban-user">Ban User</a>&nbsp;&middot;&nbsp;<a href="#" class="clear-votes">Clear Votes</a></div>','html'] );

        }

	    Enjin_Core.dropdownMenu( items, el, 'forum-user-dropdown', false );
	    
	    $('.element_dropdown_menu div[data-id] .ban-user').click( $.proxy(forum.banUser,forum) );
	    $('.element_dropdown_menu div[data-id] .ban-ip').click( $.proxy(forum.banIP,forum) );
	    $('.element_dropdown_menu div[data-id] .clear-votes').click( $.proxy(forum.clearUserVotes,forum) );
	},
	
	/**
	 * Signature editing and moderation features
	 *
	 * @param {object} event Browser click event
	 * @param {int} user_id Id of the user of the post that 
	 * @param {object} 
	 * @param {boolean} admin_access True if user has admin accss
	 * @param {boolean} signature_access True if user has signature access
	 */
	showSignatureMod: function( event, user_id, admin_access, signature_access )
	{
		var self = this;
		
		var forumModule = $(event.currentTarget).closest('.m_forum');
        var popupBox = forumModule.find('.signature-popup');
		popupBox.find('.loading').show();
		popupBox.find('.editor').hide();
	
        this.showPopupBox( popupBox, event );
		
		$.post( location.href, {m: Forum.preset_id, op:'get-signature-settings', user_id: user_id}
			   ,function(settings)
			   {	
					popupBox.find('.loading').hide();
					popupBox.find('.editor').show();
					
					var force_username = (typeof settings.force_username !== 'undefined') && (parseInt(settings.force_username,10) == 1);
					popupBox.find('input[name=force_username]').attr('checked', force_username ? 'checked' : false);
					popupBox.find('input[name=force_username]').click( function() {popupBox.find('input[name=displayname]').attr('disabled', $(this).is(':checked') ? false : 'disabled');} );

					popupBox.find('input[name=displayname]').val(typeof settings.displayname === 'undefined' ? '' : settings.displayname).attr('disabled', force_username ? false : 'disabled' );
					
					popupBox.find('input[name=signature_disabled]').attr('checked', parseInt(settings.signature_disabled,10) == 1 ? 'checked' : false);

					popupBox.find('textarea[name=signature]').text(typeof settings.signature === 'undefined' ? '' : settings.signature);
					popupBox.find('input[name=save]').click( function() {self.saveSignatureSettings(popupBox,user_id,settings);} );

					popupBox.find('.signature-preview-block').hide();
					popupBox.find('.signature-block').show();
					
					if ( popupBox.find('.bbcode-toolbar .preview-link').length == 0 )
					{
						var preview = $('<div class="preview-link"><a href="#" data-action="preview">Preview Signature</a></div>');
						preview.find('a').click( function(event) {self.signaturePreview(popupBox);return false;} );
						popupBox.find('.bbcode-toolbar').append(preview);
					}
					
					popupBox.find('.signature-preview-block a').click( function(event) {self.signatureEdit(popupBox);return false;} );
					
					if ( admin_access )
					{
						popupBox.find('.admin-only').show();
					}
					else
					{
						popupBox.find('.admin-only').hide();
						popupBox.css({top: event.pageY - forumModule.offset().top - popupBox.height() - 20});
					}

				}
				,'json');	
	},
	
	nuke: function(event, user_id, displayname) {
		if ( confirm('Are you sure you want to nuke the user ' + displayname + ' (' + user_id + ')?') ) {
			$.post('/ajax.php?s=forum', 
				   {cmd:'nuke', user_id: user_id}, 
				   function(result) {
					if ( result.success ) {
						alert('User ' + displayname + ' has been successfully nuked!');
						document.location.reload();
					} else {
						alert('Nuking failed: ' + result.error);
					}
				   },
				   'json'
				  );
		}
	},
	
	signatureEdit: function(popupBox)
	{
		popupBox.find('.signature-preview-block').hide();
		popupBox.find('.signature-block').show();
	},
	
	signaturePreview: function(popupBox)
	{
		$.post( '/ajax.php?s=bbparser'
			   ,{text:popupBox.find('textarea[name=signature]').val()}
			   ,function(text)
				{
					popupBox.find('.signature-preview-block').show();
					popupBox.find('.signature-preview').html(text);
					popupBox.find('.signature-block').hide();
				}
			  );
	},
	
	saveSignatureSettings: function(popupBox,user_id, settings)
	{
		var data = {force_username: popupBox.find('input[name=force_username]').is(':checked') ? 1 : 0
				   ,displayname: popupBox.find('input[name=displayname]').val()
				   ,signature: popupBox.find('textarea[name=signature]').val()
				   ,user_id: user_id
				   ,op: 'set-signature-settings'
				   ,m: Forum.preset_id
				   };

		if ( popupBox.find('input[name=signature_disabled]').is(':checked') )
		{
			// if the checkbox was off, and has now been ticked, flag that server must disable
			// signatures with our user id as modertor who did it
			if ( parseInt(settings.signature_disabled,10) == 0 ) 
			{ 	
				data.signature_disabled = 1;
			} 
			else
			{
				// status has not changed, so let server know it must do nothing
				// can't return a 0 here as zero means feature is deactivated!!
				data.signature_disabled = -1;
			}
		}
		else
		{
			// disable feature, i.e enable signatures for this user
			data.signature_disabled = 0;
		}
		
	   $.post(location.href, data, function() {
           location.reload(true);
       }, 'json');
	},	
	
	/**
	 * Display the popup showing the users IP/Host and username, and giving moderator options to ban this user
	 */
	showIPBanBox: function(event, preset_id, post_id, ip, host, userId, userName ) 
	{
		var forumModule = $(event.currentTarget).closest('.m_forum');
        var popupBox = forumModule.find('.ip-popup');
        
        // store the data fields needed for the ban actions to ban the right IP and user
        popupBox.attr( 'data-ip', ip );
        popupBox.attr( 'data-id', post_id );                                    
        popupBox.attr( 'data-username', userName );                                    
        
        // display the IP address and host name
        popupBox.find('.ip-name').text(ip);
        popupBox.find('.host-name').text(host);
        
        this.showPopupBox( popupBox, event );
    },
	
	/**
	 * Quote a post into the quick reply box
	 */
	quotePost: function( event, post_id )
	{
        if (event.stopPropagation) event.stopPropagation();
	    else event.cancelBubble = true; 
		
		$.post( location.href
    	       ,{m: Forum.preset_id, op: 'quote-post', post_id:post_id}
			   ,function(result)
			    {
					if ( typeof result.bbcode !== 'undefined' )
					{
						var textarea = $('#content.quicky-reply-content')[0];
						$('#content.quicky-reply-content').focus();
						if (document.selection) {
							var s = document.selection;	
							if ($.browser.msie) { // ie	
								var range = s.createRange();
								var stored_range = range.duplicate();
								stored_range.moveToElementText(textarea);
								stored_range.setEndPoint('EndToEnd', range);
								var s = stored_range.text.length - range.text.length;
			
								caretPosition = s - (textarea.value.substr(0, s).length - textarea.value.substr(0, s).replace(/\r/g, '').length);
							} else { // opera
								caretPosition = textarea.selectionStart;
							}
						} else { // gecko & webkit
							caretPosition = textarea.selectionStart;
						} 
						
						if (document.selection) {
							var newSelection = document.selection.createRange();
							newSelection.text = result.bbcode;
						} else 
						{
							var offset = caretPosition;
							textarea.value =  textarea.value.substring(0, caretPosition)  + result.bbcode + textarea.value.substring(offset, textarea.value.length);
						}
						
						// ensure editor has input focus and force a change event so the auto-resize
						// plugin will adjust the height of the widget correctly for new content
						$('#content.quicky-reply-content').focus().change();							
					}
					else
					{
						Enjin_Core.alert(result.error);
					}
			    }
			   ,'json');
	},
    
    banUser: function(event)
    {
		// get the data
		event.preventDefault();
		var template = $('.m_forum .ban_user_popup').clone();
		
		// format the data
		var data_container = $(event.target).parents('.ip-popup');
		var post_id = data_container.attr('data-id');
		var username = data_container.attr('data-username');
		template.find('input[name="post_id"]').val(post_id);

		// show the popup
		var popup_elem = new Enjin.UI.Window({
			cls: 'ban_user_popup',
			title: 'Ban ' + username,
			content: template.html(),
			hideFooter: true,
			callback: function() {},
			cancel_callback: function() {}
		});
		var popup = $('.element_popup.ban_user_popup');
		
		// close popup
		popup.find('.form_close').unbind('click').bind('click', function(event){
			event.preventDefault();
			popup_elem.cancel();
		});
		
		// form submit
		popup.find('form').unbind('submit').bind('submit', function(event){
			event.preventDefault();
			popup.find('.form_button').val('Submitting');
			
			$(this).ajaxSubmit({
				dataType: 'json',
				success: function(data){
					// build the errors
					var errors_html = '';
					if (data.error){
						$.each(data.error, function(i, message){
							errors_html += '<p>'+message+'</p>';
						});
					}
					
					if (errors_html != ''){
						popup.find('.form_error').html(errors_html);
					} else {
						Enjin_Core.alert("Ban successful");
						popup_elem.cancel();
					}
					popup.find('.form_button').val('Ok');
				}
			});
		});
		
		// datepicker
		popup.find('.expire_date input').datepicker();
    },
	
    banIP: function(event)
    {     
        var ipPopup = $(event.currentTarget).closest('.ip-popup');
        this.showBanPrompt( event, 'Are you sure you want to ban the IP ' + ipPopup.attr('data-ip') + ' from accessing this site?', 'ban-ip' );
        return false;
    },
    
    banBoth: function(event)
    {
        var ipPopup = $(event.currentTarget).closest('.ip-popup');
        this.showBanPrompt( event, 'Are you sure you want to ban both the user ' + ipPopup.attr('data-username') + 
			' and the IP ' + ipPopup.attr('data-ip') + ' from accessing this site?', 'ban-both' );
        return false;
    },
	
	banUserId: function(event)
    {
        var ipPopup = $(event.currentTarget).closest('.ip-popup');
        this.showBanPrompt( event, 'Are you sure you want to ban the user #' + ipPopup.attr('data-user-id') + ' from accessing this site?', 'ban-user-id' );
        return false;
    },
	
	banUserIdIp: function(event)
    {
        var ipPopup = $(event.currentTarget).closest('.ip-popup');
        this.showBanPrompt( event, 'Are you sure you want to ban both the user #' + ipPopup.attr('data-user-id') + 
			' and the IP ' + ipPopup.attr('data-ip') + ' from accessing this site?', 'ban-user-id-ip' );
        return false;
    },
    
    showBanPrompt: function( event, msg, op)
    {
        var ipPopup = $(event.currentTarget).closest('div[data-ip]');
        
        Enjin_Core.showPopup({message: msg,
                                callback: function(event) {
                                                			$.post( location.href
                                                			       ,{m: Forum.preset_id, op: op, post_id:ipPopup.attr('data-id')}
                                                                   ,function(response)  { 
                                                                            if ( response == "success" ) {
                                                                                Enjin_Core.alert("Ban successful");
                                                                            }
                                                                            else {Enjin_Core.alert(response);}
                                                                        }    
                                                			      ); 
                                                          }                                                			             
                             });        
    },
    
	showHidePost: function(event, post_id, admin_post_hidden)
	{
        if (event.stopPropagation) event.stopPropagation();
	    else event.cancelBubble = true; 
		
		$.post( location.href
    	       ,{m: Forum.preset_id, op: 'show-hide-post', post_id:post_id, admin_post_hidden: 1-parseInt(admin_post_hidden,10)}
			   ,function(result)
			    {
					location.reload(true);
				}
			  );	
	}
	
    /**
     * 
     */
    ,preferences: function( event )
    {
        if (event.stopPropagation) event.stopPropagation();
	    else event.cancelBubble = true; 
        
        var body = '<div class="forum-preferences"><div class="input-label">Set number of threads per page</div>';
        body += '<select id="threads_per_page">';
		var thread_counts = [15,20,25,30,35,40,45,50];
        for ( var i in thread_counts ) {body += '<option value="' + thread_counts[i] + '"' + (Forum.preferences.threads_per_page == thread_counts[i] ? ' SELECTED' : '') + '>' + thread_counts[i] + '</option>';} 
        body += '</select>';
        body += '<div class="input-label" style="margin-top:8px;">Set number of posts per page</div>';
        body += '<select id="posts_per_page">';
        for ( var i= 10; i <= 30; i+=5 ) {body += '<option value="' + i + '"' + (Forum.preferences.posts_per_page == i ? ' SELECTED' : '') + '>' + i + '</option>';} 
        body += '</select>';
        body += '<div style="padding-top:8px"><input name="auto_subscribe" type="checkbox" ' + (parseInt(Forum.preferences.auto_subscribe,10)==1 ? 'CHECKED' : '') + '> Automatically subscribe me to threads that I reply to.</div>';
        body += '<div style="padding-top:8px"><input name="hide_new_post_indicator" type="checkbox" ' + (parseInt(Forum.preferences.hide_new_post_indicator,10)==1 ? 'CHECKED' : '') + '> Turn off new posts indicator.</div>';
        body += '</div>';
        
        Enjin_Core.showPopup({message: "Personal Forum Settings",
                                message1: body,
                                button_continue: "Save Changes",
                                callbackFirst: true,
                                callback: function(event) {
															var data  = {m: Forum.preset_id        
                                                			         ,op:'forum-preferences'
                                                			         ,preset_id:Forum.preset_id
                                                			         ,threads_per_page: $('.forum-preferences #threads_per_page').val()
                                                			         ,posts_per_page: $('.forum-preferences #posts_per_page').val()
                                                			         ,auto_subscribe: $('.forum-preferences input[name=auto_subscribe]:checked').length == 1 ? 1 : 0
																	 ,hide_new_post_indicator: $('.forum-preferences input[name=hide_new_post_indicator]:checked').length == 1 ? 1 : 0
                                                			        };
                                                			$.post( location.href
                                                			       ,data
                                                                   ,function(response)  { 
                                                                            if ( response == "success" ) 
																			{
                                                                                Enjin_Core.alert("Preferences Updated");
																				$.extend(Forum.preferences,data);
                                                                            }
                                                                            else {Enjin_Core.alert(response);}
                                                                        }    
                                                			      ); 
                                                          }                                                			             
                             });
    },
    
    replyReviewQuote: function(event,username)
    {
        var message = $(event.currentTarget).closest('.reply').find('.bbcode').html();
        message = '[quote=' + username + ']' + message + '[/quote]';

        $('#post-content').focus();

        // based on jquery.markitup.js function insert(block) line 479        
		if ( document.selection ) 
		{
			var newSelection = document.selection.createRange();
			newSelection.text = message;
		} 
		else 
	    {
	        var textarea = $('#post-content').get(0);
	        var caretPosition = textarea.selectionStart;
	        var selection = textarea.value.substring(caretPosition, textarea.selectionEnd);
			textarea.value =  textarea.value.substring(0, caretPosition) + message + textarea.value.substring(caretPosition + selection.length, textarea.value.length);
		}   
    },
    
    showPostHistory: function(event)
    {                       
        $(event.currentTarget).closest('.show-link').addClass('hidden');
        $(event.currentTarget).closest('.post-review').find('.replies').removeClass('hidden');
        $(event.currentTarget).closest('.post-review').find('.hide-review').removeClass('hidden');
    },
    
    hidePostHistory: function(event)
    {                        
        $(event.currentTarget).closest('.post-review').find('.show-link').removeClass('hidden');
        $(event.currentTarget).closest('.post-review').find('.replies').addClass('hidden');
        $(event.currentTarget).closest('.post-review').find('.hide-review').addClass('hidden');
    },
    
    /**
     * 
     */
    threadSubscription: function( el, event, threadId, title )
    {
        var el = $(el);
        var subscribed = parseInt( el.attr('data-subscribed'), 10 );
        if ( subscribed )
        {
       	$.post( location.href, {m: Forum.preset_id, op: 'unsubscribe-thread', thread_id:threadId}
                   ,function()  {el.attr('value','Subscribe').attr('data-subscribed',0);}    
        	      ); 
        }
        else
        {
            this.subscribePopup( el, event, threadId, true, title );
        }
    },
    
    /**
     * 
     */
    forumSubscription: function( el, event, forumId, title )
    {
        var el = $(el);
        var subscribed = parseInt( el.attr('data-subscribed'), 10 );
        if ( subscribed )
        {
       	$.post( location.href, {m: Forum.preset_id, op: 'unsubscribe-forum', forum_id:forumId}
                   ,function()  {el.attr('value','Subscribe').attr('data-subscribed',0);}    
        	   ); 
        }
        else
        {
            this.subscribePopup( el, event, forumId, false, title );
        }
    },
    
    subscribePopup: function( el, event, id, isThread, title )
    {
        if (event.stopPropagation) event.stopPropagation();
	    else event.cancelBubble = true;         
                                                                            
        var body = '<div class="forum-subscribe"><div class="title">' + Enjin_Core.filterOutput( title.length > 45 ? title.substr(0,45) + '...' : title ) + '</div>';
        body += '<div class="item"><input type="radio" value="email" name="forum-subscribe-option" checked> Instant Email Notification</div>';
        body += '<div class="note">Send out instant notification to your toolbar &amp; email.</div>';
        body += '<div class="item"><input type="radio" value="notify" name="forum-subscribe-option"> Toolbar Notification only</div>';
        body += '<div class="note">Send out instant notification to your toolbar only.</div>';
        body += '</div>';
        var params = {m: Forum.preset_id        
			          ,op:'subscribe-' + ( isThread ? 'thread' : 'forum' )
			         };
        params[ isThread ? 'thread_id' : 'forum_id' ] = id;

        Enjin_Core.showPopup({message: "Subscribe to Forum" + ( isThread ? " Thread" : ""),            
                                message1: body,
                                button_continue: "Subscribe",
                                callbackFirst: true,
                                callback: function() {
                                                        params.subscription = $('.forum-subscribe input:checked').val();
                                            			$.post( location.href       
                                            			       ,params
                                                               ,function(response)  {el.attr('value','Unsubscribe').attr('data-subscribed',1);}    
                                            			      ); 
                                                      }                                                			             
                             });
            
        
    },

	getPermalink: function(post_id) {
		var url = document.location.href.split('/viewthread/');
		var thread = url[1].split('/');
		return url[0] + '/viewthread/' + thread[0] + '/post/' + post_id + '#p' + post_id;
	},
        
    /**
     * Shows permalink to this post so the user can copy it to the clipboard
     */                 
    permalink: function( event, post_id )
    { 
		var forumModule = $(event.currentTarget).closest('.m_forum');
        var popupBox = forumModule.find('.permalink-popup');
        
        // store the data fields needed for the ban actions to ban the right IP and user
        var url = this.getPermalink(post_id);
		popupBox.find('input').val(url);
        
        this.showPopupBox( popupBox, event );    
        popupBox.find('input').select();
    },
	
	vote: function(el) {
		var self = this;
		var post_id = el.closest('tr[post_id]').attr('post_id');
		var votetype_id = el.attr('data-votetypeid');
        var preset_id = el.closest(".m_forum").attr("data-preset_id");
		var data = {
            cmd: 'vote',
            post_id: post_id,
            preset_id: preset_id,
            votetype_id: votetype_id
        };
		$.post('/ajax.php?s=forum', 
				data, 
				function(result) { 
					if ( result.success ) {
						el.closest(".post-voting-bar").attr("data-has-voted", "1");
                        el.closest(".post-voting-bar").find(".vote-types").css("display", "none");
                        el.closest(".post-voting-bar").find(".votes").css("display", "inline-block");

						self.updateVotesAndProfile(el,result);
					} else {
						switch (result.error) {
							case 'more_positive':
								msg = 'You must give out more positive votes than negative votes in total.<br/>Give out more positive votes to continue voting negative votes.';
								break;
							case 'user_negative_limit':
								msg = 'You have reached the limit of the number of negative votes you can cast on this specific user for today.';
								break;
							case 'user_neutral_limit':
								msg = 'You have reached the limit of the number of neutral votes you can cast on this specific user for today.';
								break;
							case 'user_positive_limit':
								msg = 'You have reached the limit of the number of positive votes you can cast on this specific user for today.';
								break;
							case 'daily_negative_limit':
								msg = 'You have reached the limit of the number of negative votes you can cast for today.';
								break;
							case 'daily_neutral_limit':
								msg = 'You have reached the limit of the number of neutral votes you can cast for today.';
								break;
							case 'daily_positive_limit':
								msg = 'You have reached the limit of the number of positives votes you can cast for today.';
								break;
                            case 'same_user':
                                msg = 'You can\'t cast votes upon yourself.';
                                break;
                            case 'insert_error':
                                msg = "There was a problem. Please try again later.";
                                break;
                            case 'minimum_zero':
                                msg = "You can't cast this vote on this user.";
                                break;
							default:
								msg = result.error;
						}
						Enjin_Core.alert(msg);
					}
				}, 
			   'json'); 		
	},
	
	voteUndo: function(el) {
		var self = this;
		var post_id = el.closest('tr[post_id]').attr('post_id');
        var preset_id = el.closest(".m_forum").attr("data-preset_id");
		var data = {
            cmd: 'vote-undo',
            post_id: post_id,
            preset_id: preset_id
        };
		$.post('/ajax.php?s=forum', data, function(result) {
            if ( result.success ) {
                var bar = el.closest(".post-voting-bar");
                var hover_mode = (bar.attr("data-mode") == 'hover');

				bar.attr("data-has-voted", "0");
                if (hover_mode) {
                    if (result['votes'].length == 0) {
                        bar.find(".vote-types").css("display", "inline-block");
                        bar.find(".votes").css("display", "none");
                    }else{
                        bar.find(".vote-types").css("display", "none");
                        bar.find(".votes").css("display", "inline-block");
                    }
                }else{
                    bar.find(".vote-types").css("display", "inline-block");
                    bar.find(".votes").css("display", "inline-block");
                }

                self.updateVotesAndProfile(el,result);
            }
        }, 'json');
	},

    voteRemove: function(el) {
        var self = this;
        var post_id = el.closest(".vote-type-container").attr("data-post-id");
        var voted_user_id = el.closest(".vote").attr("data-user-id");
        var data = {
            cmd: 'vote-remove',
            preset_id: Forum.preset_id,
            post_id: post_id,
            voted_user_id:voted_user_id
        };

        $.post('/ajax.php?s=forum', data, function(result) {
            if (result.success) {
				var row = $("tr[post_id='"+post_id+"']");
				var post_bottom = row.find(".post-bottom");

				self.updateVotesAndProfile(post_bottom,result);

				$('.element_popup').enjinHideStandardPopup();
            }
        }, 'json');
    },
	
	updateVotesAndProfile: function(el,result) {
		var container = el.closest('.row');
        var bar = container.find(".post-voting-bar");
		var votes = bar.find('.votes');
        var post_user_id = container.find('.c.profile').attr('data-userid');
        var post_profile = $('.c.profile[data-userid='+post_user_id+']');
        var hover_mode = (bar.attr("data-mode") == 'hover');
        var show_list = parseInt(bar.attr("data-show-list"));

        votes.html("");
		if ( result.votes.length > 0 ) {
			for ( var n in result.votes ) {
				var vote = result.votes[n];
                var vote_name = Enjin_Core.teaser(Enjin_Core.filterOutput(vote['vote_name']), 10);
                var vote_total = vote['total'];
                var avatars_html = '';
                for (var p in vote['user_avatars']) {
                    var avatar_html = '<div class="user">' + vote['user_avatars'][p] + '</div>';
                    avatars_html += avatar_html + ' ';
                }

				var html = '' +
                    '<div class="vote">' +
                        '<div class="tooltip">' +
                            '<div class="tooltip-container">' +
                                '<div class="top">' +
                                    '<div class="vote_name">'+ vote_name +'</div>' +
                                    '<div class="vote_total">x '+ vote_total +'</div>' +
                                '</div>' +
                                '<div class="bottom">' +
                                    avatars_html +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                        '<span class="vote-icon">' +
                            '<img src="'+vote.vote_icon_src+'" />' +
                        '</span>' +
                        '<span class="text grey">' +
                            ' x&nbsp;</span>' +
                        '<span class="text">'+vote.total+'</span>' +
                    '</div>';
                votes.append(html);
			}
		}

        if (hover_mode) {
            var html = '';
            if (show_list) html = '<div class="list"><span>List</span></div>';
            html += '<div class="vote-undo"><span>Undo</span></div>';
            votes.append(html);
        }

        var user_votes = parseInt(result.level.user_votes);
        var user_votes_with_sign = result.level.user_votes_with_sign;

        var user_level_reached = (result['level']['votelevel_id'] != null);
        if (result['settings']['show_level_bar'] && user_level_reached) {
            post_profile.find(".vote-level").css("display","block");
        }else{
            post_profile.find(".vote-level").css("display","none");
        }

		// update the post owners profile on siebar, this will update all instances of his profile
		// showing on any posts he has made currently rendered on the page
        post_profile.find('.forum-votes .right').text(user_votes_with_sign);

        if (result['settings']['show_level_name'] == '1') {
            post_profile.find('.vote-level .level-name').css("display","block").text(Enjin_Core.filterOutput(result.level.level_name));
        }else{
            post_profile.find('.vote-level .level-name').css("display","none").text(Enjin_Core.filterOutput(result.level.level_name));
        }

        if (result['settings']['use_level_name'] == '1') {
            post_profile.find('.vote-level .level-number').text(Enjin_Core.teaser(Enjin_Core.filterOutput(result.level.level_name.toUpperCase()), 10));
        }else{
            post_profile.find('.vote-level .level-number').text('LEVEL ' + result.level.level);
        }

        // coloring of vote count (red/green)
        if (user_votes > 0) {
            post_profile.find('.forum-votes .right').addClass("positive").removeClass("negative");
        }else if (user_votes == 0) {
            post_profile.find('.forum-votes .right').removeClass("negative").removeClass("positive");
        }else{
            post_profile.find('.forum-votes .right').addClass("negative").removeClass("positive");
        }

        // level bar percentage
		var bar_percentage = result.level.level_perc;
        post_profile.find('.element_progressbar .current').css("width", bar_percentage+"%");

        // level bar active color
        post_profile.find('.element_progressbar .current .cleft').attr("style", "");
        post_profile.find('.element_progressbar .current .cright').attr("style", "");
        if (typeof result.level.bar_active_color !== 'undefined' && result.level.bar_active_color != '') {
            post_profile.find('.element_progressbar .current .cleft').attr("style", "background-color: " + result.level.bar_active_color + ";");
            post_profile.find('.element_progressbar .current .cright').attr("style", "background-color: " + result.level.bar_active_color + ";");
        }

        if (result['level']['on_last_level']) {
            post_profile.find(".vote-level").attr("data-tooltip", "Highest level reached");
        }else{
            var votes_needed = result['level']['votes_needed'];
            var next_level_name = result['level']['next_level_name'];
            var vote_label = result['settings']['vote_label'];
            var tooltip = votes_needed + " " + vote_label + " till next level (" + next_level_name + ")";
            post_profile.find(".vote-level").attr("data-tooltip", tooltip);
        }

        // website points
        if (typeof result['website_points'] !== 'undefined') {
            var website_points_el = post_profile.find('.website-points .right');
            var content = result['website_points'];
            if (content > 0) {
                content = '+' + content;
                if (!website_points_el.hasClass("positive")) {
                    website_points_el.addClass("positive");
                }
            }else{
                website_points_el.removeClass("positive");
            }
            website_points_el.html(content);
        }

        // user tags
        if (typeof result['tags'] !== 'undefined') {
            post_profile.find('.cell .tag').remove();
            post_profile.find('.cell').append(result['tags']);
        }

        if (result['votes'].length == 0) {
			bar.attr("data-has-votes", "0");
        }else{
			bar.attr("data-has-votes", "1");
        }

        var has_votes = parseInt(bar.attr("data-has-votes"));
        var has_voted = parseInt(bar.attr("data-has-voted"));

        if (has_votes) {
            bar.find(".list").css("display", "inline-block");
        }else{
            bar.find(".list").css("display", "none");
        }

        if (has_voted) {
            bar.find(".vote-undo").css("display", "inline-block");
        }else{
            bar.find(".vote-undo").css("display", "none");
        }

        // avatar dropdown
        post_profile.find(".avatar-hover-trigger").attr("data-votes", user_votes);

        $('.m_forum .post-voting-bar .votes .vote').bindVoteTooltip();
        $('.m_forum .post-voting-bar .vote-types .vote-type').bindVoteTypeTooltip();
        $('.m_forum .profile_cell_info .vote-level').bindVoteLevelTooltip();
		$('.m_forum .profile_cell_info .tooltip').bindProfileStatsTooltip();
    },
	
	listVotes: function( el ) {
		var self = this;
		var post_id = el.closest('tr[post_id]').attr('post_id');
        var preset_id = el.closest(".m_forum").attr("data-preset_id");
		var settings = {
            get: '/ajax.php?s=forum&cmd=vote-list&preset_id='+preset_id+'&post_id='+post_id
        };
		$.fn.enjinShowStandardPopup(settings);
	},
	
	/** 
	 * Clears all the votes from this specific post
	 */  	 
	resetVotes: function( el, post_id )
	{
		if(confirm("Are you sure you want to reset all votes in this post?")) {
			$.post(location.href, {m: Forum.preset_id, op: 'reset-votes', post_id: post_id},
				function(response){
					// "success" means vote was processed, but there are no updates to the UI required
					// otherwise the response is a JSON encoded dataset with new UI display values
					if ( response == "success" )
					{
						location.reload(true);
					}
				}
			);
		}
	},    
	
	
	/**
	 * Clears all the votes in this module from every post from this specific user
	 */
	clearUserVotes: function( event )
	{
		if(confirm("Are you sure you want to clear all votes this user has received?")) {
			var avatarHover = $(event.currentTarget).closest('div[data-userid]');

			$.post(location.href, {m: Forum.preset_id, op: 'clear-user-votes', user_id: avatarHover.attr('data-userid')},
				function(response){
					// "success" means vote was processed, but there are no updates to the UI required
					// otherwise the response is a JSON encoded dataset with new UI display values
					if ( response == "success" )
					{
						location.reload(true);
					}
				}
			);
		}
		return false;
	},
	
	unhidePost: function( el, post_id )
	{       
	    var triggerBlock = $('tr[data-hidden-post-trigger=' + post_id + ']');
	    var action = ( triggerBlock.hasClass('hidden') ? 'hide' : 'unhide' );
		$.post(location.href, {m: Forum.preset_id, op: 'unhide-post', post_id: post_id, postAction: action},
			function(response){
                // "success" means vote was processed, but there are no updates to the UI required    
                // otherwise the response is a JSON encoded dataset with new UI display values
                if ( response == "success" ) 
                {           
                    // if we marked the post as unhidden, then we must not show the trigger block
                    // any more, admin still sees the vote admin conrols though
                    if ( action == 'unhide' )
                    {   
                        triggerBlock.addClass('hidden');		        
                        el.text('Hide Post');
                    }
                    // we re-hide a previously unhidden post, so show the post trigger block again
                    else    
                    {                       
                        triggerBlock.removeClass('hidden');
						$('tr[post_id=' + post_id + ']').addClass('hidden');
                        el.text('Unhide Post');
                    }
                }				    
			}
		);
	}, 
	        
    showPopupBox: function( popupBox, event )
    {
		var forumModule = $('.m_forum');
		popupBox.css({
			top: event.pageY - forumModule.offset().top - popupBox.height() - 20,
			left: event.pageX - forumModule.offset().left - (popupBox.width()/2)
		});        
		popupBox.fadeIn(160);
		
		event.stopPropagation();
		
		$(document).one("click", function(event) {popupBox.fadeOut(160);});
		popupBox.find('a.cancel').click(function(event) {
			popupBox.fadeOut(160);return false;
		});
		
		popupBox.bind('click', function(event) {event.stopPropagation();});        
    },
    
	report: function(event, preset_id, post_id) {
		var limit_length = 256;
		event.stopPropagation();
		
		Enjin_Core.showPromptPopup({
			message: 'Report this post',
			width: 300,
			callback: function(msg) 
			{
				msg = $.trim(msg).substr(0, limit_length);				
				if (msg != '') 
				{
					// get the page id
					var url_path = window.location.pathname;
					var forum_page = 1;
					if (url_path.indexOf('/page/') !== -1) {
						var paths = url_path.split("/");
						var page_index;
						$.each(paths, function(key, value) {
							if (value == 'page') {
								page_index = key;
							}
						});

						if (typeof paths[page_index+1] !== 'undefined') {
							forum_page = paths[page_index+1];
						}
					}

					$.post(Forum.ajaxurl, {
							cmd: 'report-forum', 
							preset_id: preset_id,
							post_id: post_id,
							reason: msg,
							page: forum_page
						},
			    		function (response) {
							if (response.error != '') {
								Enjin_Core.alert(response.error);
							} else {
								
							}
						}, 'json');
				}
			}
		});
	},
	
	createThreadPreview: function(el) {
		var html = '<div>';
		html += '<div class="post">'+Enjin_Core.filterOutput(el.attr('data-post'))+'</div>';
		html += '<div class="stats time">'+el.attr('data-time')+'</div>';
		html += '<div class="stats views">'+el.attr('data-views')+' Views / '+el.attr('data-replies')+' Replies</div>';
		html += '<div class="stats lastpost">Last Post by	'+el.attr('data-username')+' ('+el.attr('data-lastposttime')+')</div>';
		html += '</div>';
		
		Enjin_Core.bindTooltip(el, html, 'm_forum thread-preview-hover');
	}
}



$(document).ready(function(){
    
    // show the custom filter window
    $('.m_forum .filter-elem .gear-icon').bind('click', function(event){
        if ($('.m_forum .filter-custom-html').is(':hidden')){
            $('.m_forum .filter-custom-html').css({
                top: $(this).position().top + 20,
                left: $(this).position().left - 60
            }).slideDown();            
        } else {
            $('.m_forum .filter-custom-html').slideUp();
        }
        event.preventDefault();
    });
    
    $('body').bind('click', function(event){
        var target = $(event.target);
        if (target.is('.filter-custom-cancel')){
            
            // hide the custom filter window
            target.parents('.element_popup').slideUp();
            event.preventDefault();
        }
    });
    
    
    // not allowed in threads
    $('.m_forum .thread-view').bind('click', function(event){
        if ($(this).attr('data-notallowed') == 1){
            Enjin.UI.alert("You do not have thread view access");
            event.preventDefault();
        }
    });
    
    
    // limit posting
    $('.m_forum .post-button').bind('click', function(event){
        if ($(this).attr('data-notallowed') == 1){
            Enjin.UI.alert("You need at least " + $(this).attr('data-minimum-posts') + " posts before posting in this forum");
            event.preventDefault();
        }
    });
    
    
    // auto save
    if ($('.m_forum .forum-post-content').length) {
		var editpost = $(".m_forum").hasClass("editpost");
		if ($.localStorage && !editpost) {
			var post_content = $(".m_forum .forum-post-content");
			var thread_id = post_content.attr('data-thread-id');
			var cookie_name = 'forum_post_content_draft_' + thread_id;

			setInterval(function () {
				var new_content = post_content.val();
				if (new_content) {
					$.localStorage.set(cookie_name, new_content);
				}
			}, 1000 * 3);  // every 3 seconds instead of 10 because we are using it for saving the quick reply content too

			if (!$.cookie("forum_post_successful")) {
				var stored_content = $.localStorage.get(cookie_name);
				post_content.val(stored_content);
			}
		}
    }


	if ($.cookie("forum_post_successful")) {
		if ($.localStorage) {
			var thread_id = $.cookie("forum_post_successful");
			$.localStorage.remove("forum_post_content_draft_" + thread_id);
			$.cookie('forum_post_successful', '', {expires: -1, path: '/'});
		}
	}
    
    
    // collapse categories
    $('.m_forum .expand-category').bind('click', function(event){
        var target = $(event.target);
        if (target.is('.expand-category')){
            // clicking on the category name expands it
            target = target.children('.collapse-category');
        }
		if (target.hasClass('icon-caret-down')){
			// clicking on the down arrow collapses it
			target.removeClass('icon-caret-down').addClass('icon-caret-right');
			target.parents('.block-title').next().fadeOut();
			target.saveCollapsibleCategory(1);

		} else {
			// clicking on the right arow expands it
			target.removeClass('icon-caret-right').addClass('icon-caret-down');
			target.parents('.block-title').next().fadeIn();
			target.saveCollapsibleCategory(0);
		}
    });
    
    
    // label events
    $('body').bind('change', function(event){
        var target = $(event.target);
        if (target.is('.m_forum .label-add')){
            
            // adding labels
            var value = target.val();        
            if (value == 0){
                return;
            }

            // set the data
            var name = target.children(':selected').html();
            var color = target.children(':selected').attr("data-color");
            var style = target.children(':selected').attr("data-style");
            var hover_text = target.children(':selected').attr("data-hover-text");
            var parent = target.parents('.thread-labels');
            var element = parent.find('.thread-label-sample .forum-label').clone();

            // add the data
            element.attr('data-id', value).attr("title", hover_text).addClass("forum-label-" + style);
            element.children('span').html(name);
            element.children('input').val(value);

            // add the color        
            if (style == "hidden"){
                element.css("color", color);
            } else if (style == "stroke-square" || style == "stroke-rounded" || style == "stroke-circle"){
                element.css("border-color", color);
            } else {
                element.css("background-color", color);
            }

            // add the element
            parent.find('.thread-label-list').append(element);
            
            // disable the option and select the empty option
            target.children(':selected').attr('disabled', 'disabled');
            target.val('');
        }
    }).bind('click', function(event){
        var target = $(event.target);
        if (target.is('.m_forum .forum-label em')){
            
            // remove the label
            target.parent().remove();
            
            // enable the option
            var value = target.parent().attr('data-id');
            $(".m_forum #form-elem-labels option:disabled").each(function(){
                if ($(this).val() == value){
                    $(this).removeAttr('disabled');
                    return false;
                }
            });
            
        } else if (target.is('.m_forum .label-cancel')){
            
            // hide the labels window
            target.parents('.element_popup').slideUp();
            if ($(this).val() == 'add-labels' || $(this).val() == 'remove-labels'){
                $('.m_forum .bulk-moderation-action').val('');
            }
            event.preventDefault();
        }
    }).bind('submit', function(event){
        var target = $(event.target);
        if (target.is('.m_forum .add-labels-form')){
            
            // save the added labels
            // get the selected threads and place the json data into an input hidden and then continue with the form sbumit
            var posts = [];
            $('.thread-id-element:checked').each(function(){
                posts.push($(this).val());
            });
            $('.m_forum #label-posts').val($.toJSON(posts));
        }
    });
    
    
    // show add label window
    $('.m_forum .bulk-moderation-action').bind('change', function(){
        if ($(this).val() == 'add-labels' || $(this).val() == 'remove-labels'){
            $('.m_forum .add-labels-container').hide();
            $(this).addRemoveLabelsWindow();
        } else {
            $('.m_forum .label-cancel').trigger('click');
        }
    });
    
    $('.m_forum .link-label-thread a').bind('click', function(event){
        $(this).addRemoveLabelsWindow('set');
        event.preventDefault();
    });
    
    
    // post admin links
    var forum = new Forum();
	$('.m_forum .ip-popup .links .ban-user').click( $.proxy(forum.banUser,forum) );
    $('.m_forum .ip-popup .links .ban-ip').click( $.proxy(forum.banIP,forum) );
	$('.m_forum .ip-popup .links .ban-both').click( $.proxy(forum.banBoth,forum) );
	
    $('.m_forum .ip-popup .links .ban-user-id').click( $.proxy(forum.banUserId,forum) );
    $('.m_forum .ip-popup .links .ban-user-id-ip').click( $.proxy(forum.banUserIdIp,forum) );
    
    $('body').bind('click', function(event){
        var target = $(event.target);
        if (false == target.is('.cancel')){
            $('.m_forum .element_popup.admin-container').hide(); 
        }        
    });
    $('.m_forum .post-admin').bind('click', function(){
        $('.m_forum .element_popup.admin-container').hide();
        Enjin_Core.dropdownMenuNoHtml($(this), $(this).next(), {
            top: 14, 
            left: -6
        });
        return false;
    });
    
    
    // edit history popup
    $('.m_forum .edit-history-button').bind('click', function(){
        Enjin_Core.showWindowPopup({
            title: $(this).attr('data-title'),
            cls: 'edit-history-window',
            hideFooter: true,
            noTabs: true,
            content: '<div class="loading">Loading...</div>'
        });
        
        var popup = $('.element_popup:last');
        $.get($(this).attr('data-url'), function(html){
			if(!$.trim(html)) html = '<div class="record"><div class="record-description">There is no edit history for this post.</div></div>';
			popup.find('.content').html(html);
			popup.find('.window-header').appendTo('.element_popup:last .popup_window_title');
        });
    });
    
    $('body').bind('click', function(event){
        var target = $(event.target);
        if (target.is('.element_popup.edit-history-window .post-lock')){
            
            // lock/unlock the post editing
            target.parent().hide().siblings().show();
            $.get(target.attr('href'));
            event.preventDefault();
        }
    });
    
    
    // notices - init
    var notice = {
        base: $('.m_forum').attr('data-base-url'),
        location: $('.m_forum').attr('data-location'),
        forum_id: $('.m_forum').attr('forum_id')
    }
    
    
    // notices - events
    $('body').bind('click', function(event){
        var target = $(event.target);
        if (target.is('.m_forum .forum-notices .more')){
            
            // expand content
            var content = target.prev();
            if (target.html() == 'SEE MORE'){
                content.children('.more-content').show();
                content.children('.less-content').hide();
                target.html('SEE LESS');
            } else {
                content.children('.more-content').hide();
                content.children('.less-content').show();
                target.html('SEE MORE');
            }
            event.preventDefault();
            
        } else if (target.is('.m_forum .forum-notices .page-turn')){
            
            // pagination
            if (target.hasClass('disabled') == false){
                var url = notice.base + '/notices/1?ajax=1&location=' + notice.location + '&forum_id=' + notice.forum_id + '&page=' + target.attr('data-page');
                $.fn.loadNoticePage(url);
            }
            
        } else if (target.is('.m_forum .forum-notices .delete')){
            
            // delete notice
            url = notice.base + '/notice-delete/' + target.attr('data-id') + '?ajax=1&location=' + notice.location + '&forum_id=' + notice.forum_id + '&page=1';
            $.fn.loadNoticePage(url);
        }
    });
    
    
    // show the dislikes window
    $('.m_forum .votes-dislikes').bind('click', function(){
        var popup = $(this).closest('.votes-dislikes').next();
        if (popup.css('visibility') === 'hidden') {
            popup.css('visibility', 'visible');
        } else {
            popup.css('visibility', 'hidden');
        }
    });
	
	
	// view more awards
	$('.m_forum .awards .view_more').bind('click', function(event){
		event.preventDefault();
		$(this).hide().prevAll('.hide').removeClass('hide');
	});
	
	
	// mentions - form submit
	$(document).on('submit', '.m_forum.viewthread .tabbox.reply form.section, '+
		'.m_forum.newthread form[name="postform"], '+
		'.m_forum.newreply form[name="postform"], '+
		'.m_forum.editpost form[name="postform"]', function(){
		//Enjin_Core.disableButton(this, 4);
		
		var input = $(this).find('.forum-post-content');
		input.mentionsInput('val', function(value){
			input.val(value);
		});
	});

    $(".m_forum .post-voting-bar").each(function() {
        var bar = $(this);
        var votes_width = bar.find(".votes").width();
        var votetypes_width = bar.find(".vote-types").width();
        var bar_width = bar.width();
        var exceeds = (votes_width+votetypes_width > bar_width);
        var has_voted = parseInt(bar.attr("data-has-voted"));
        var has_votes = parseInt(bar.attr("data-has-votes"));
        var has_options = parseInt(bar.attr("data-has-options"));
        var show_list = parseInt(bar.attr("data-show-list"));
        var hover_mode = false;

        if (!exceeds) {
			bar.attr("data-mode", "normal");
			hover_mode = true;
        }else{
            // we need to move the undo/list buttons into the votes container
            // when set to hover_mode. This is because we want the buttons to show
            // inline next to the list of votes
            bar.find(".list").remove();
            bar.find(".vote-undo").remove();
            if (show_list) bar.find(".vote-types").append('<div class="list"><span>List</span></div>');
            if (show_list) bar.find(".votes").append('<div class="list"><span>List</span></div>');
            bar.find(".votes").append('<div class="vote-undo"><span>Undo</span></div>');
            if (has_voted) {
                bar.find(".vote-undo").css("display","inline-block");
                bar.find(".votes").find(".list").css("display","inline-block");
            }
        }
    });

    $(document).on("mouseenter", ".m_forum .post-voting-bar-container", function() {
        toggleVoteHover('mouseenter', $(this));
    }).on("mouseleave", ".m_forum .post-voting-bar-container", function() {
        toggleVoteHover('mouseleave', $(this));
    });

    function toggleVoteHover(action, container) {
        var bar = container.find(".post-voting-bar");
        var has_voted = parseInt(bar.attr("data-has-voted"));
        var has_votes = parseInt(bar.attr("data-has-votes"));
        var has_options = parseInt(bar.attr("data-has-options"));
        var hover_mode = (bar.attr("data-mode") == 'hover');

        if (hover_mode) {
            if (action == 'mouseenter') {
                if (has_options && has_votes && !has_voted) {
                    bar.find(".votes").css("display","none");
                    bar.find(".vote-types").css("display","inline-block");
                    if (hover_mode) {
                        bar.find(".vote-types .list").css("display","inline-block");
                    }else{
                        bar.find(".list").css("display","inline-block");
                    }
                }
            }else if (action == 'mouseleave') {
                if (has_options && has_votes && !has_voted) {
                    bar.find(".votes").css("display","inline-block");
                    bar.find(".vote-types").css("display","none");
                    bar.find(".list").css("display","none");
                }
            }
        }
    }

    $('.m_forum .post-voting-bar .votes .vote').bindVoteTooltip();
    $('.m_forum .post-voting-bar .vote-types .vote-type').bindVoteTypeTooltip();
    $('.m_forum .profile_cell_info .vote-level').bindVoteLevelTooltip();
	$('.m_forum .profile_cell_info .tooltip').bindProfileStatsTooltip();
});


// load the notice page
$.fn.loadNoticePage = function(url){
	$('.m_forum .forum-notices .title, .m_forum .forum-notices .content, .m_forum .forum-notices .page-number').addClass('blurred');

	$.get(url, function(content){
		$('.m_forum .forum-notices-container').html(content);
		$('.m_forum .forum-notices .title, .m_forum .forum-notices .content, .m_forum .forum-notices .page-number').removeClass('blurred');
	});
}


// save the collapsible category state
$.fn.saveCollapsibleCategory = function(state){
    $.post('/ajax.php?s=forum', {
        cmd: 'save-collapsible-category',
        preset_id: $(this).parents('.m_forum').attr('data-preset-id'),
        category_id: $(this).attr('data-category-id'),
        state: state
    });
};


// show the add/remove labels window
$.fn.addRemoveLabelsWindow = function(status) {
	var status = (typeof status !== 'undefined' ? status : 'add');
    if ($('.m_forum .add-labels-container').is(':hidden')){
        // change the text depending on the add/remove action
        var window = $('.m_forum .add-labels-container');
        if ($(this).val() == 'remove-labels'){
            window.find('#form-elem-labels option:first').html(' - Select Label To Remove - ');
            window.find('.action-field').html('Removed Labels');
            window.find('input[type=submit]').val('Remove Labels');
            window.find('input[name=action]').val('remove');            
        } else {    // add-labels
            window.find('#form-elem-labels option:first').html(' - Select Label To Add - ');
            window.find('.action-field').html('Added Labels');
            window.find('input[type=submit]').val('Add Labels');
            window.find('input[name=action]').val('add');
        }

        if (status == 'set') {
            window.find('input[type=submit]').val('Set Labels');
		}

        window.find('.thread-label-list').empty();
        window.find('#form-elem-labels').val('').children(':disabled').removeAttr('disabled');

        $.each($('.m_forum .contentbox.posts .block-title .forum-label'), function (index, value) {
        	value = $(value);
        	if (typeof value.attr('data-tag-id') === 'undefined') return;
            $('#form-elem-labels').val(value.attr('data-tag-id')).trigger('change');
		});
        
        // show the window
        $('.m_forum .add-labels-container').css({
            top: $(this).position().top + 33,
            left: $(this).position().left + 6
        }).slideDown();            
    } else {
        $('.m_forum .add-labels-container').slideUp();
    }
}

$.fn.bindVoteTooltip = function() {
    if (jQuery().enjinTooltip) {
		var css_class = 'forum_vote_tooltip large';
		if ($(this).find(".tooltip").attr("data-mode") == 'hide_avatars') {
			css_class = 'forum_vote_tooltip';
		}

        $(this).enjinTooltip({
            attr: 'data-tooltip',
            class: css_class,
            escape: false,
            container: $(this).find(".tooltip")
        });
    }
}

$.fn.bindVoteTypeTooltip = function() {
    if (jQuery().enjinTooltip) {
        $(this).enjinTooltip({
            attr: 'data-tooltip',
            class: 'forum_votetype_tooltip',
            escape: true
        });
    }
}

$.fn.bindVoteLevelTooltip = function() {
    if (jQuery().enjinTooltip) {
        $(this).enjinTooltip({
            attr: 'data-tooltip',
            class: 'forum_votelevel_tooltip',
            escape: true
        });
    }
}

$.fn.bindProfileStatsTooltip = function() {
	if (jQuery().enjinTooltip) {
		$(this).enjinTooltip({
			attr: 'data-tooltip',
			class: 'forum_profile_stats_tooltip',
			escape: true
		});
	}
}